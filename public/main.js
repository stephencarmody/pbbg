import ASHES from './ashes.js'
import './assets/dirt/dirt.js'
import './assets/rock/rock.js'
import './assets/water/water.js'



ASHES.noise.seed( '8HV6vOdd1n7DwJmr' )

const __game__ = window.__game__ = {}
__game__.global_n = ASHES.noise( 0, 0, 1 )
__game__.noise = ( x, y, frequency ) => { return ASHES.noise( ( ( x + 50000 ) / 100000 ) * frequency, ( ( y + 50000 ) / 100000 ) * frequency ) }

ASHES.viewport.zoom( 1 )


// let n50 = __game__.noise( cell.x, cell.y, 50 )
// let n20000 = __game__.noise( cell.x, cell.y, 20000 )
// let n30000 = __game__.noise( cell.x, cell.y, 30000 )
// let n60000 = __game__.noise( cell.x, cell.y, 60000 )

// let rotation = ( n30000 + 1 ) * Math.PI


// if ( n20000 < -.8 ) {
//     if ( n20000 > -.84 ) return ASHES.Object.add( { sprite: 'plant', x: cell.x, y: cell.y, sprite_scale_x: .3 + ( n30000 + 1 ) * 1, sprite_scale_y: .3 + ( n30000 + 1 ) * 1, opacity: 1, rotation: rotation, layer: 1, inViewUpdates: [ inViewUpdates.wind ] } )
//     //if ( n20000 > .7 ) return ASHES.Object.add( { sprite: { image: '../3/assets/brush' }, x: cell.x, y: cell.y, sprite_scale_x: .2 + ( n30000 + 1 ) * .6, sprite_scale_y: .2 + ( n30000 + 1 ) * .6, opacity: .8, rotation: rotation, layer: 1, inViewUpdates: [ inViewUpdates.wind ] } )
//     return ASHES.Object.add( { sprite: n30000 < -.35 ? 'stone1' : n30000 < .35 ? 'stone2' : 'stone3', x: cell.x, y: cell.y, rotation: rotation, layer: -1 } )
// }



const inViewUpdates = {
    wind: node => {
        let positional_noise = __game__.noise( node.x + ASHES.time, node.y + ASHES.time, 2 )
        let n = positional_noise * .5 + __game__.global_n * .5
        node.rotation_delta = n * .2
        node.sprite_scale_x_delta = node.sprite_scale_y_delta = ( 1 + n ) * 3
    }
}

function createSmoke( origin_node, dx, dy ) {
    let pos = ASHES.Object.absolutePosition( origin_node )
    pos.x += ( dx || 0 )
    pos.y += ( dy || 0 )
    let smoke = ASHES.Object.add( { name: 'smoke', sprite: 'smoke', x: pos.x, y: pos.y, layer: 3, rotation: Math.random() * Math.PI * 2, rotationLock: true, opacity: .15 - Math.random() * .13 } )
    ASHES.Event.addListener( smoke, 'preRender', () => {
        let age = ASHES.time - smoke.createdTime
        smoke.opacity = Math.max( smoke.opacity - age * .00000001, 0 )
        smoke.sprite_scale_x = smoke.sprite_scale_y = .5 + age * .001
        ASHES.Object.translate( smoke, smoke.x + __game__.noise( ASHES.time, 0, 1 ) * .2 * ( 2 - smoke.opacity ), smoke.y + __game__.noise( 0, ASHES.time, 1 ) * .2 * ( 2 - smoke.opacity ) )
    } )
    setTimeout( () => {
        return ASHES.Object.remove( smoke )
    }, Math.random() * 20000 )
    return smoke
}

function createEmber( origin_node, dx, dy ) {
    let pos = ASHES.Object.absolutePosition( origin_node )
    pos.x += ( dx || 0 )
    pos.y += ( dy || 0 )
    let ember = ASHES.Object.add( { sprite: Math.random() > .5 ? 'ember1' : 'ember2', x: pos.x, y: pos.y, layer: origin_node.layer + 1 } )
    ASHES.Event.addListener( ember, 'preRender', () => {
        let n = __game__.noise( ember.x * 9999 + ASHES.time, ember.y * 9999 + ASHES.time, 1 ) * .5 + __game__.global_n * .5
        ember.translate_x_delta += n * .5
        ember.translate_y_delta += ( ( n + 1 ) * .5 ) * -.3 - ( .7 - ember.opacity )
        ember.opacity = Math.max( ember.opacity - Math.random() * .02, 0 )
    } )
    setTimeout( () => {
        return ASHES.Object.remove( ember )
    }, Math.random() * 10000 )
}

const characterTypeInfo = {
    'tank': { image: '../2/assets/tank', base_speed: .02 },
    'melee': { image: '../2/assets/melee', base_speed: .03 },
    'ranged': { image: '../2/assets/ranged', base_speed: .04 },
}

function createCharacter( options ) {
    options = options || {}
    let types = Object.keys( characterTypeInfo )
    let type = options.type ? options.type : types[ Math.floor( Math.random() * types.length ) ]
    let info = characterTypeInfo[ type ]
    let character = ASHES.Object.add( Object.assign( {
        name: 'character',
        width: 16,
        height: 16,
        physical: true,
        rotationLock: true,
        speed: info.base_speed,
        range: 128,
        attackspeed: .5,
        autoAttack: {
            target: null,
            cooldown: 1000,
            lastAttack: 0
        }
    }, options ), null, 'nearest' )
    if ( !character ) return
    ASHES.Object.add( character, { sprite: 'helmet' } )
    if ( 'ranged' === type ) {
        ASHES.Object.add( character, { x: -14, sprite: 'bow' } )
    } else if ( 'melee' === type ) {
        ASHES.Object.add( character, { x: -15, sprite: 'sword' } )
    } else if ( 'tank' === type ) {
        ASHES.Object.add( character, { x: -11, sprite: 'shield' } )
    }
    setHealth( character, 100, -16 )
    return character
}

function setHealth( node, health, healthbarYOffset ) {
    node.health = health
    node.healthbar = {}
    node.healthbar.black = ASHES.Object.add( node, { y: healthbarYOffset, sprite: 'line', sprite_scale_x: .8, sprite_scale_y: 2 } )
    node.healthbar.green = ASHES.Object.add( node.healthbar.black, { sprite: 'greenline', sprite_scale_x: node.healthbar.black.sprite_scale_x, sprite_scale_y: 2 } )
}

function createCampfire( x, y ) {
    let campfire = ASHES.Object.add( {
        sprite: 'campfire',
        x: x,
        y: y,
        width: 24,
        height: 24,
        physical: true,
        layer: -1
    }, null, 'nearest' )
    createFlame( campfire )
    createFlame( campfire, 3 * ( Math.random() - .5 ), 3 * ( Math.random() - .5 ) )
    return campfire
}

function createFlame( origin_node, dx, dy ) {
    let x = origin_node.x + ( dx || 0 )
    let y = origin_node.y + ( dy || 0 )
    let flame = ASHES.Object.add( origin_node, { name: 'flame', sprite: 'flame', x: 0, y: 0, opacity: .5, rotation: Math.random() * 2 * Math.PI } )
    ASHES.Event.addListener( flame, 'preRender', () => {
        let x = ( flame.id + flame.x ) * 9999 + flame.translate_x_delta + ASHES.time
        let y = ( flame.id + flame.y ) * 9999 + flame.translate_y_delta + ASHES.time
        let x_noise = __game__.noise( x, 0, 100 )
        let y_noise = __game__.noise( 0, y, 100 )
        let xy_noise = __game__.noise( x, y, 100 )
        flame.translate_x_delta = x_noise * 2
        flame.translate_y_delta = y_noise * 2
        flame.rotation_delta = xy_noise * .25 + __game__.global_n * .25
        if ( xy_noise > .8 ) createEmber( flame, 10 * ( __game__.noise( x, y, 1000 ) * .5 + __game__.global_n * .5 ), 10 * y_noise )
        if ( xy_noise > .9 ) createSmoke( flame )
    } )
    return flame
}


ASHES.Event.addListener( ASHES, 'update', () => {
    __game__.global_n = __game__.noise( ASHES.time * 10, 0, 1 )
} )


const attackable = [ 'character' ]

const mousemove = () => {
    if ( ASHES.mouse.left || ASHES.mouse.right ) mousedown()
}

const mousedown = () => {
    let mouse = ASHES.mouse
    let nodes = ASHES.cells.getXY( mouse.world.x, mouse.world.y )
    for ( let i = 0, l = nodes.length; i < l; i++ ) {
        let node = nodes[ i ]
        if ( node !== __game__.player && -1 < attackable.indexOf( node.name ) ) return autoAttack( __game__.player, node )
    }
    let dx = mouse.world.x - __game__.player.x
    let dy = mouse.world.y - __game__.player.y
    let min = 16 / ASHES.viewport.scale
    if ( min < Math.abs( dx ) || min < Math.abs( dy ) ) {
        __game__.player.autoAttack.target = null
        ASHES.Object.move( __game__.player, mouse.world )
    }
}

ASHES.Event.addListener( ASHES, 'mousemove', mousemove )
ASHES.Event.addListener( ASHES, 'mousedown', mousemove )

window.addEventListener( 'keydown', event => {
    if ( event.key === 'q' ) {
        //shootArrow( __game__.player, ASHES.mouse.world )
    }
} )


__game__.campfire = createCampfire( 0, 0 )
__game__.player = createCharacter( { type: 'ranged', x: 0, y: 0 } )
ASHES.viewport.follow( __game__.player )

ASHES.run()
