import Stats from './Stats.js'

const ASHES = {
    cellsize: 16,
    fontfamily: 'Calibri',
    fontsize: '10pt',
    nextGUID: 0,
    sprites: {},
    images: {
        ready: {},
        loading: {},
        maxsize: 0
    },
    following: null,
    animations: [],
    time: 0,
    elapsed: 0,
    mouse: {
        screen: { x: 0, y: 0 },
        world: { x: 0, y: 0 },
        left: false,
        middle: false,
        right: false
    }
}


ASHES.run = () => {
    
    function resize() {
        ASHES.canvas.width = .5 * window.innerWidth
        ASHES.canvas.height = .5 * window.innerHeight
    }

    resize()
    window.addEventListener( 'resize', resize )
    update( 0 )
    
}


ASHES.Event = {
    addListener: ( object, type, listener ) => {
        void 0 === object._listeners && ( object._listeners = {} )
        let arr = object._listeners
        void 0 === arr[ type ] && ( arr[ type ] = [] )
        arr[ type ].push( listener )
    },

    removeListener: ( object, type, listener ) => {
        if ( void 0 === object._listeners ) return
        let arr = object._listeners[ type ]
        if ( void 0 === arr ) return
        let i = arr.indexOf( listener )
        if ( - 1 === i ) return
        arr.splice( i, 1 )
    },

    dispatch: ( object, type, ...args ) => {
        if ( void 0 === object._listeners ) return
        let arr = object._listeners[ type ]
        if ( void 0 === arr ) return
        arr = arr.slice( 0 )
        for ( let i = 0, l = arr.length; i < l; i++ ) arr[ i ].call( object, ...args )
    }
}



ASHES.Vec2 = ( x, y ) => {
    return {
        x: x || 0,
        y: y || 0
    }
}

ASHES.Vec2.set = ( vec, x, y ) => {
    vec.x = x
    vec.y = y
    return vec
}

ASHES.Vec2.rotate = ( vec, radians, origin ) => {
    let sin = Math.sin( radians )
    let cos = Math.cos( radians )
    if ( origin ) {
        vec.x -= origin.x
        vec.y -= origin.y
    }
    let x = vec.x
    vec.x = vec.x * cos - vec.y * sin
    vec.y = x * sin + vec.y * cos
    if ( origin ) {
        vec.x += origin.x
        vec.y += origin.y
    }
    return vec
}

ASHES.Vec2.len = ( vec ) => {
    return Math.sqrt( vec.x * vec.x + vec.y * vec.y )
}

ASHES.Vec2.distance = ( a, b ) => {
    let x = a.x - b.x
    let y = a.y - b.y
    return Math.sqrt( x * x + y * y )
}





ASHES.noise = ( x, y ) => {
    let a = ASHES.noise, c = a.g2, n = a.perm, p = a.perm123, e = a.grad3
    a = ( x + y ) * a.f2
    let f = x + a >> 0, b = y + a >> 0, d = ( f + b ) * c
    a = x - ( f - d )
    d = y - ( b - d )
    let q = 0, r = 1
    a > d && ( q = 1, r = 0 )
    let t = a - q + c, u = d - r + c, v = a - 1 + 2 * c
    c = d - 1 + 2 * c
    f &= 255
    let w = b & 255, g = .5 - a * a - d * d, h = .5 - t * t - u * u, k = .5 - v * v - c * c, l = 0, m = 0, z = 0
    0 <= g && ( b = p[ f + n[ w ] ], g *= g, l = g * g * ( e[ b ] * a + e[ b + 1 ] * d ) )
    0 <= h && ( b = p[ f + q + n[ w + r ] ], h *= h, m = h * h * ( e[ b ] * t + e[ b + 1 ] * u ) )
    0 <= k && ( b = p[ f + 1 + n[ w + 1 ] ], k *= k, z = k * k * ( e[ b ] * v + e[ b + 1 ] * c ) )
    return 70 * ( l + m + z )
}

ASHES.noise.f2 = 0.5 * ( Math.sqrt( 3.0 ) - 1.0 )
ASHES.noise.g2 = ( 3.0 - Math.sqrt( 3.0 ) ) / 6.0
ASHES.noise.perm = new Uint8Array( 512 )
ASHES.noise.perm123 = new Uint8Array( 512 )
ASHES.noise.grad3 = new Float32Array( [
    1, 1, 0, -1, 1, 0, 1, -1, 0, -1, -1, 0,
    1, 0, 1, -1, 0, 1, 1, 0, -1, -1, 0, -1,
    0, 1, 1, 0, -1, 1, 0, 1, -1, 0, -1, -1
] )

ASHES.noise.seed = function ( seed ) {
    let d = ASHES.noise, e = d.perm, b = 0, c = 0
    d = d.perm123
    for ( ; c < seed.length; ++c ) b = ( b << 5 ) - b + seed.charCodeAt( c ), b |= 0
    seed = 256
    for ( c = new Uint8Array( 256 ); seed--; ) c[ seed ] = 256 * ( ( b = 1E4 * Math.sin( b ) ) - Math.floor( b ) ) | 0
    for ( seed = 512; seed--; ) e[ seed ] = c[ seed & 255 ], d[ seed ] = e[ seed ] % 12 * 3
}

ASHES.randomString = length => {
    return Array( length ).join().split( ',' ).map( () => { return 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'.charAt( Math.floor( 62 * Math.random() ) ) } ).join( '' )
}





ASHES.Object = ( options ) => {
    let object = Object.assign( {
        id: ASHES.nextGUID++,
        createdTime: ASHES.time,
        name: null,
        x: 0,
        y: 0,
        translate_x_delta: 0,
        translate_y_delta: 0,
        width: 1,
        height: 1,
        rotation: 0,
        rotation_delta: 0,
        layer: 0,
        spatial: true,
        physical: false,
        rotationLock: false,
        cells: [],
        children: [],
        speed: 0,
        inview: false,
        opacity: 1,
        sprite_scale_x: 1,
        sprite_scale_x_delta: 0,
        sprite_scale_y: 1,
        sprite_scale_y_delta: 0,
        inViewUpdates: []
    },
        options
    )
    ASHES.Object.setSprite( object, options.sprite )
    return object
}

ASHES.Object.inview = []


ASHES.Object.containsPoint = ( object, point ) => {
    let sin = Math.sin( -object.rotation )
    let cos = Math.cos( -object.rotation )
    let _x = point.x - object.x
    let y = point.y - object.y
    let x = _x * cos - y * sin
    y = _x * sin + y * cos
    let width = .5 * object.width * object.sprite_scale_x
    let height = .5 * object.height * object.sprite_scale_y
    return !( x < -width || x > width || y < -height || y > height )
}


ASHES.Object.setSprite = ( object, sprite ) => {

    if ( typeof sprite === "string" && ASHES.sprites.hasOwnProperty( sprite ) )
        object.sprite = ASHES.sprites[ sprite ]

    else if ( !sprite )
        delete object.sprite
    
    else
        object.sprite = sprite

    return object.sprite

}


ASHES.Object.setSpriteColour = ( object, colour ) => {
    let sprite = object.sprite
    function ready() {
        object.canvas = document.createElement( 'canvas' )
        let width = sprite.canvas.width
        let height = sprite.canvas.height
        object.canvas.width = width
        object.canvas.height = height
        let ctx = object.canvas.getContext( '2d' )
        ctx.imageSmoothingEnabled = false
        ctx.fillStyle = colour
        ctx.fillRect( 0, 0, width, height )
        ctx.globalCompositeOperation = 'destination-in'
        ctx.drawImage( object.canvas, 0, 0, width, height )
    }
    sprite.ready ? ready() : ASHES.Event.addListener( sprite, 'ready', ready )
}


ASHES.Object.closestUnblockedPosition = ( object, origin ) => {
    let layer = 1
    let leg = 0
    let pos = ASHES.Vec2( origin.x, origin.y )
    let x = 0
    let y = 0
    while ( ASHES.Object.blockedByCells( object, ASHES.cells.overlapped( pos, object.width * object.sprite_scale_x, object.height * object.sprite_scale_y, object.rotation ) ) ) {
        switch ( leg ) {
            case 0: ++x; if ( x == layer )++leg; break;
            case 1: ++y; if ( y == layer )++leg; break;
            case 2: --x; if ( -x == layer )++leg; break;
            case 3: --y; if ( -y == layer ) { leg = 0; ++layer; } break;
        }
        pos.x = origin.x + x * ASHES.cellsize
        pos.y = origin.y + y * ASHES.cellsize
    }
    return pos
}


ASHES.Object.add = ( parent, object, behaviour ) => {

    if ( !object ) {
        object = typeof parent.id === 'undefined' ? ASHES.Object( parent ) : parent
        let cells = ASHES.cells.overlapped( object, object.width * object.sprite_scale_x, object.height * object.sprite_scale_y, object.rotation )
        if ( 'nearest' === behaviour && ASHES.Object.blockedByCells( object, cells ) ) {
            let pos = ASHES.Object.closestUnblockedPosition( object, object )
            ASHES.Vec2.set( object, pos.x, pos.y )
            cells = ASHES.cells.overlapped( pos, object.width * object.sprite_scale_x, object.height * object.sprite_scale_y, object.rotation )
        }
        ASHES.Object.calculateOccupiedCells( object, cells )
        return object
    }

    object = typeof object.id === 'undefined' ? ASHES.Object( object ) : object

    if ( -1 === parent.children.indexOf( object ) ) {
        parent.children.push( object )
        if ( parent.inview ) {
            ASHES.Object.addToView( object )
        }
        if ( parent.isCell ) {
            ASHES.Event.dispatch( ASHES, 'cellchildrenchanged', parent )
        }
    }

    object.parent = parent

    return object

}


ASHES.Object.addNearestUnblocked = ( object ) => {
    return ASHES.Object.add( object, null, 'nearest' )
}


ASHES.Object.remove = ( parent, object ) => {
    if ( !object ) {
        for ( let i = 0; i < parent.cells.length; i++ )
            ASHES.Object.remove( parent.cells[ i ], parent )
        return
    }
    if ( object.moveAnimationId ) ASHES.Animation.stop( object.moveAnimationId )
    ASHES.Object.removeFromView( object )
    let i = parent.children.indexOf( object )
    if ( -1 < i ) {
        parent.children.splice( i, 1 )
        if ( parent.isCell ) ASHES.Event.dispatch( ASHES, 'cellchildrenchanged', parent )
    }
}


ASHES.Object.blockedByCells = ( object, cells ) => {
    for ( let i = 0, l = cells.length; i < l; i++ ) {
        let cell = cells[ i ]
        if ( cell.physical && cell.physical !== object ) return true
    }
    return false
}


ASHES.Object.calculateOccupiedCells = ( object, cells ) => {

    if ( cells )
        // clone cells array so caller can pass a list that wont be tampered with (for example object.cells )
        cells = cells.slice( 0 )
    else
        cells = ASHES.cells.overlapped( object, object.width * object.sprite_scale_x, object.height * object.sprite_scale_y, object.rotation )
    
    let nodecellschanged = false
    let nodecells = object.cells

    for ( let i = 0; i < nodecells.length; i++ ) {
        let cell = nodecells[ i ]
        let t = cells.indexOf( cell )
        if ( -1 < t ) {
            cells.splice( t, 1 )
            continue
        }
        nodecells.splice( i, 1 )
        if ( cell.physical === object ) cell.physical = null
        ASHES.Event.dispatch( ASHES, 'cellchildrenchanged', cell )
        nodecellschanged = true
        i--
        t = cell.children.indexOf( object )
        if ( - 1 < t ) cell.children.splice( t, 1 )
    }

    for ( let i = 0, l = cells.length; i < l; i++ ) {
        let cell = cells[ i ]
        nodecells.push( cell )
        nodecellschanged = true
        if ( 0 > cell.children.indexOf( object ) ) {
            cell.children.push( object )
            if ( object.physical ) cell.physical == object
            ASHES.Event.dispatch( ASHES, 'cellchildrenchanged', cell )
        }
    }

    for ( let i = 0; i < nodecells.length; i++ ) {
        if ( object.physical ) nodecells[ i ].physical = object
        ASHES.Event.dispatch( ASHES, 'cellchildrenchanged', nodecells[ i ] )
    }

    if ( nodecellschanged )
        ASHES.Event.dispatch( object, 'nodecellschanged', object )

    let inview = false

    for ( let i = 0, l = nodecells.length; i < l; i++ )
        if ( nodecells[ i ].inview ) { inview = true; break }
    
    if ( inview !== object.inview ) {
        inview ? ASHES.Object.addToView( object ) : ASHES.Object.removeFromView( object )
        object.inview = inview
    }

}


ASHES.Object.translate = ( object, x, y, speed, onComplete ) => {
    if ( x === object.x && y === object.y ) { onComplete && onComplete(); return }
    let dx = x - object.x
    let dy = y - object.y
    object.rotationLock || ( object.rotation = Math.atan2( dy, dx ) )
    let duration = Math.sqrt( dx * dx + dy * dy ) / speed
    if ( !duration ) {
        ASHES.Vec2.set( object, x, y )
        ASHES.Object.calculateOccupiedCells( object )
        ASHES.following === object && ASHES.viewport.set( object.x, object.y )
        ASHES.Event.dispatch( object, 'translate', object )
        onComplete && onComplete()
        return
    }
    x = object.x
    y = object.y
    object.moveAnimationId = ASHES.Animation.start( duration, lerp => {
        if ( object.inview ) ASHES.viewport.dirty = true
        ASHES.Vec2.set( object, x + lerp * dx, y + lerp * dy )
        ASHES.Object.calculateOccupiedCells( object )
        ASHES.following === object && ASHES.viewport.set( object.x, object.y )
        ASHES.Event.dispatch( object, 'translate', object )
    }, onComplete )
}


ASHES.Object.move = ( object, target, onComplete ) => {
    object.moveAnimationId && ASHES.Animation.stop( object.moveAnimationId )
    if ( 0 === object.speed || ( target.x === object.x && target.y === object.y ) ) { onComplete && onComplete(); return }
    let dx = target.x - object.x
    let dy = target.y - object.y
    let hx = .5 * object.width * object.sprite_scale_x
    let hy = .5 * object.height * object.sprite_scale_y
    let corners
    if ( dx <= 0 && dy <= 0 )
        corners = [ { x: 0, y: 0 }, { x: -hx, y: -hy }, { x: hx, y: -hy }, { x: -hx, y: hy } ]
    else if ( dx <= 0 && dy > 0 )
        corners = [ { x: 0, y: 0 }, { x: -hx, y: -hy }, { x: -hx, y: hy }, { x: hx, y: hy } ]
    else if ( dx > 0 && dy <= 0 )
        corners = [ { x: 0, y: 0 }, { x: hx, y: -hy }, { x: -hx, y: -hy }, { x: hx, y: hy } ]
    else
        corners = [ { x: 0, y: 0 }, { x: hx, y: hy }, { x: -hx, y: hy }, { x: hx, y: -hy } ]
    for ( let i = 0; i < corners.length; i++ ) {
        let corner = corners[ i ]
        let b = { x: object.x + dx + corner.x, y: object.y + dy + corner.y }
        let cells = ASHES.cells.ray( { x: object.x + corner.x, y: object.y + corner.y }, b, object )
        if ( 0 === cells.length ) continue
        let physical = cells[ cells.length - 1 ].physical
        if ( physical && physical !== target && physical !== object ) {
            // a physical object obscures straight line to target, have to find a path around
            object.followingpath = findpath( object, target.x, target.y )
            function followpath() {
                if ( ( target.x === object.x && target.y === object.y ) || 0 === object.followingpath.length ) { onComplete && onComplete(); return }
                let p = object.followingpath.pop()
                let dx = p.x - object.x
                let dy = p.y - object.y
                if ( 0 === dx && 0 === dy ) return followpath()
                ASHES.Object.translate( object, object.x + dx, object.y + dy, object.speed, followpath )
            }
            return followpath()
        }
    }
    // straight line is not obscured, just translate to target
    ASHES.Object.translate( object, target.x, target.y, object.speed, onComplete )
}

ASHES.Object.draw = ( object ) => {
    ASHES.ctx.save()
    ASHES.ctx.translate( object.x + object.translate_x_delta, object.y + object.translate_y_delta )
    if ( object.rotation || object.rotation_delta ) ASHES.ctx.rotate( object.rotation + object.rotation_delta )
    if ( object.opacity < 1 ) ASHES.ctx.globalAlpha = object.opacity
    if ( object.sprite && object.sprite.ready ) {
        let sprite = object.sprite
        let sprite_half_width = sprite.half_width * object.sprite_scale_x + object.sprite_scale_x_delta
        let sprite_half_height = sprite.half_height * object.sprite_scale_y + object.sprite_scale_y_delta
        ASHES.ctx.drawImage( object.canvas || sprite.canvas, object.sprite.x * sprite_half_width - sprite_half_width, object.sprite.y * sprite_half_height - sprite_half_height, 2 * sprite_half_width, 2 * sprite_half_height )
    }
    ASHES.ctx.restore()
}

ASHES.Object.blockedByCell = ( object, cell ) => {
    return cell.physical && cell.physical !== object
}

ASHES.Object.removeFromView = ( object ) => {
    if ( !object.inview ) return
    object.inview = false
    let index = ASHES.Object.inview.indexOf( object )
    if ( -1 !== index ) ASHES.Object.inview.splice( index, 1 )
    for ( let i = 0; i < object.children.length; i++ )
        ASHES.Object.removeFromView( object.children[ i ] )
    ASHES.viewport.dirty = true
}

ASHES.Object.addToView = ( object ) => {
    if ( object.inview ) return
    object.inview = true
    let index = ASHES.Object.inview.indexOf( object )
    if ( -1 === index ) insert( ASHES.Object.inview, object, comparator )
    for ( let i = 0; i < object.children.length; i++ )
        ASHES.Object.addToView( object.children[ i ] )
    ASHES.viewport.dirty = true
}

ASHES.Object.absolutePosition = ( object ) => {
    let pos = ASHES.Vec2( object.x, object.y )
    while ( object.parent ) {
        object = object.parent
        pos.x += object.x
        pos.y += object.y
    }
    return pos
}





ASHES.Sprite = ( options ) => {
    options = options || {}
    let name = options.name || options.image || null
    if ( name && ASHES.sprites.hasOwnProperty( name ) ) return ASHES.sprites[ name ]
    let sprite = Object.assign( {
        x: 0,
        y: 0,
        ready: false
    }, options )
    sprite.canvas = document.createElement( 'canvas' )
    
    if (options.width) {
        sprite.canvas.width = options.width
        sprite.canvas.height = options.height
        sprite.half_width = .5 * sprite.canvas.width
        sprite.half_height = .5 * sprite.canvas.height
    }

    sprite.ctx = sprite.canvas.getContext( '2d' )
    sprite.ctx.imageSmoothingEnabled = false

    ASHES.Sprite.setText( sprite, options )
    
    sprite.name = name
    ASHES.sprites[ name ] = sprite
    
    if ( !options.image ) {
        ASHES.Sprite.repaint( sprite )
        sprite.ready = true
        return sprite
    }

    let ready = e => {
        if ( !ASHES.images.ready[ name ] ) {
            delete ASHES.images.loading[ name ]
            ASHES.images.ready[ name ] = e.target
        }
        sprite.image = ASHES.images.ready[ name ]
        if ( sprite.image.width > ASHES.images.maxsize ) {
            ASHES.images.maxsize = sprite.image.width
            ASHES.viewport.setOverdraw( ASHES.images.maxsize )
        }
        if ( sprite.image.height > ASHES.images.maxsize ) {
            ASHES.images.maxsize = sprite.image.height
            ASHES.viewport.setOverdraw( ASHES.images.maxsize )
        }
        sprite.canvas.width = sprite.image.width
        sprite.canvas.height = sprite.image.height
        sprite.half_width = .5 * sprite.canvas.width
        sprite.half_height = .5 * sprite.canvas.height
        sprite.ready = true
        ASHES.Sprite.repaint( sprite )
        if ( options.ready ) options.ready( sprite )
        ASHES.Event.dispatch( sprite, 'ready', sprite )
        return sprite
    }
    if ( ASHES.images.ready[ name ] ) return ready()
    if ( !ASHES.images.loading[ name ] ) {
        ASHES.images.loading[ name ] = new Image
        ASHES.images.loading[ name ].addEventListener( 'load', ready )
        ASHES.images.loading[ name ].src = options.image
    } else {
        ASHES.images.loading[ name ].addEventListener( 'load', ready )
    }
    return sprite
}

ASHES.Sprite.repaint = ( sprite ) => {
    let width = sprite.canvas.width
    let height = sprite.canvas.height
    let ctx = sprite.ctx
    ctx.clearRect( 0, 0, width, height )
    sprite.fill && ( ctx.fillStyle = sprite.fill, ctx.fillRect( 0, 0, width, height ) )
    sprite.image && ctx.drawImage( sprite.image, 0, 0, width, height )
    sprite.border && ( ctx.beginPath(), ctx.strokeStyle = sprite.bordercolour, ctx.lineWidth = .5 * sprite.border, ctx.rect( .5, .5, width - 1, height - 1 ), ctx.stroke() )
    if ( sprite.text ) {
        ctx.font = sprite.font
        ctx.fillStyle = sprite.colour
        ctx.textAlign = sprite.textAlign
        ctx.textBaseline = sprite.textBaseline
        ctx.fillText( sprite.text, 0, 0 )
    }
}

ASHES.Sprite.setText = ( sprite, options ) => {
    if ( !options ) {
        delete sprite.text
        return
    }
    sprite.text = options.text
    sprite.font = options.font || ASHES.fontsize + ' ' + ASHES.fontfamily
    sprite.colour = options.font || '#000000'
    sprite.textAlign = options.font || 'left'
    sprite.textBaseline = options.font || 'top'
    let ready = () => { ASHES.Sprite.repaint( sprite ) }
    sprite.ready ? ready() : ASHES.Event.addListener( sprite, 'ready', ready )
}



ASHES.Sprite.loadSprites = ( images, onloaded ) => {

    let sprites = []

    for ( let i = 0; i < images.length; i++)
        sprites.push( ASHES.Sprite( { image: images[ i ] } ) )

    const new_cells_before_ready = []
    
    function onnewcell( cell ) {
        new_cells_before_ready.push( cell )
    }

    ASHES.Event.addListener( ASHES, 'newcell', onnewcell )

    for ( let i = 0; i < sprites.length; i++)
        ASHES.Event.addListener( sprites[ i ], 'ready', spriteReady )

    function spriteReady() {
        
        if ( !sprites.allready ) {
            for ( let i = 0; i < sprites.length; i++)
                if ( !sprites[ i ].ready ) return
            ASHES.Event.removeListener( ASHES, 'newcell', onnewcell )
            sprites.allready = true
            onloaded( sprites, new_cells_before_ready )
        }

    }

}




ASHES.Animation = {}



function heuristic( a, b ) {
    return Math.abs( b.x - a.x ) + Math.abs( b.y - a.y )
}

function comparator( node_a, node_b ) {
    let layer = node_a.layer - node_b.layer
    return layer === 0 ? node_a.y - node_b.y : layer
}

function insert( array, item, comparator, noduplicates ) {
    if ( !array.length ) return array.push( item )
    let high = array.length - 1
    let cmp = comparator( array[ high ], item )
    if ( cmp < 0 || ( cmp === 0 && !noduplicates ) ) return array.push( item )
    cmp = comparator( array[ 0 ], item )
    if ( cmp > 0 || ( cmp === 0 && !noduplicates ) ) return array.splice( 0, 0, item )
    let i
    for ( let low = 0; low <= high && ( i = ( low + high ) / 2 | 0, cmp = comparator( array[ i ], item ), cmp !== 0 ); )
        cmp < 0 ? low = i + 1 : high = i - 1
    if ( noduplicates ) {
        do {
            cmp = comparator( array[ i ], item )
            if ( cmp !== 0 ) break
            if ( array[ i ] === item ) return
        } while ( ++i < array.length )
    }
    array.splice( i, 0, item )
}



ASHES.cells = {}

ASHES.cells.inview = []

ASHES.cells.overlapped = ( pos, width, height, rotation ) => {
    // this is not a true 100% occupation. it's just returning cells at the centre plus edges
    let overlapped = [ ASHES.cells.ray( pos, pos )[ 0 ] /* centrecell, topleftcell, ..., bottomrightcell */ ]
    let a = { x: 0, y: 0 }
    let b = { x: 0, y: 0 }
    let hx = .5 * width
    let hy = .5 * height
    ASHES.Vec2.rotate( ASHES.Vec2.set( a, pos.x - hx, pos.y - hy ), rotation, pos )
    ASHES.Vec2.rotate( ASHES.Vec2.set( b, pos.x - hx, pos.y + hy - 1 ), rotation, pos )
    let leftray = ASHES.cells.ray( a, b )
    ASHES.Vec2.rotate( ASHES.Vec2.set( a, pos.x + hx - 1, pos.y - hy ), rotation, pos )
    ASHES.Vec2.rotate( ASHES.Vec2.set( b, pos.x + hx - 1, pos.y + hy - 1 ), rotation, pos )
    let rightray = ASHES.cells.ray( a, b )
    let length = Math.min( leftray.length, rightray.length )
    for ( let n = 0; n < length; n++ ) {
        let left = leftray[ n ]
        let right = rightray[ n ]
        let row = ASHES.cells.ray( left, right )
        for ( let i = 0, l = row.length; i < l; i++ ) {
            let cell = row[ i ]
            0 > overlapped.indexOf( cell ) && overlapped.push( cell )
        }
        if ( n > 0 ) {
            let lastleft = leftray[ n - 1 ]
            let lastright = rightray[ n - 1 ]
            row = ASHES.cells.ray( { x: ( lastleft.x + left.x ) * .5, y: ( lastleft.y + left.y ) * .5 }, { x: ( lastright.x + right.x ) * .5, y: ( lastright.y + right.y ) * .5 } )
            for ( let i = 0, l = row.length; i < l; i++ ) {
                let cell = row[ i ]
                0 > overlapped.indexOf( cell ) && overlapped.push( cell )
            }
        }
    }
    return overlapped
}


ASHES.cells.getColRow = ( col, row ) => {

    let key = col + ',' + row
    let exists = ASHES.cells.hasOwnProperty( key )
    let cell = exists ? ASHES.cells[ key ] : ASHES.cells[ key ] = {
        isCell: true,
        col: col,
        row: row,
        x: col * ASHES.cellsize,
        y: row * ASHES.cellsize,
        children: [],
        inview: false
    }

    if ( !exists )
        ASHES.Event.dispatch( ASHES, 'newcell', cell )

    return cell

}


ASHES.cells.getXY = ( x, y ) => {
    return ASHES.cells.getColRow( Math.floor( ( x / ASHES.cellsize ) + .5 ), Math.floor( ( y / ASHES.cellsize ) + .5 ) )
}

ASHES.cells.getCellsWithinRadius = ( origin, radius ) => {
    let cells = []
    for ( let y = origin.y - radius, maxy = origin.y + radius; y <= maxy; y += ASHES.cellsize ) {
        for ( let x = origin.x - radius, maxx = origin.x + radius; x <= maxx; x += ASHES.cellsize ) {
            let cell = ASHES.cells.getXY( x, y )
            radius >= ASHES.Vec2.distance( origin, cell ) && cells.push( cell )
        }
    }
    return cells
}

ASHES.cells.getObjectsWithinRadius = ( origin, radius ) => {
    let objects = []
    let cells = ASHES.cells.getCellsWithinRadius( origin, radius )
    for ( let i = 0, l = cells.length; i < l; i++ ) {
        let children = cells[ i ].children
        for ( let t = 0, n = children.length; t < n; t++ )
            objects.push( children[ t ] )
    }
    return objects
}




class Heap {
    constructor() {
        this.items = []
    }
    push( item ) {
        item.index = this.items.length
        this.items.push( item )
        this.bubbleUp( this.items.length - 1 )
    }
    pop() {
        let items = this.items
        let result = items[ 0 ]
        let end = items.pop()
        0 < items.length && ( items[ 0 ] = end, this.sinkDown( 0 ) )
        return result
    }
    remove( item ) {
        let items = this.items
        let l = items.length
        for ( let i = 0; i < l; i++ )
            if ( items[ i ] == item ) {
                item = items.pop()
                if ( i == l - 1 ) break
                items[ i ] = item
                this.bubbleUp( i )
                this.sinkDown( i )
                break
            }
    }
    size() {
        return this.items.length
    }
    bubbleUp( index ) {
        let items = this.items
        let item = items[ index ]
        let score = item.score
        for ( ; 0 < index; ) {
            let i = Math.floor( ( index + 1 ) / 2 ) - 1
            let parent = items[ i ]
            if ( score >= parent.score ) break
            items[ i ] = item
            item.index = i
            items[ index ] = parent
            parent.index = index
            index = i
        }
    }
    sinkDown( index ) {
        for ( let items = this.items, l = items.length, item = items[ index ], score = item.score; ; ) {
            let child2 = 2 * ( index + 1 )
            let child1 = child2 - 1
            let swap = null
            let child1Score
            child1 < l && ( child1Score = items[ child1 ].score, child1Score < score && ( swap = child1 ) )
            child2 < l && items[ child2 ].score < ( null === swap ? score : child1Score ) && ( swap = child2 )
            if ( null === swap ) break
            items[ index ] = items[ swap ]
            items[ index ].index = index
            items[ swap ] = item
            index = item.index = swap
        }
    }
}

function findpath( object, x, y ) {
    findpath.id = findpath.id ? findpath.id + 1 : 1
    let start = { x: object.x, y: object.y }
    let end = { x: x, y: y }
    let startcell = ASHES.cells.getXY( start.x, start.y )
    let endcell = ASHES.cells.getXY( x, y )
    if ( startcell === endcell ) return [ end ]
    let heap = new Heap
    startcell.parent = null
    startcell.h = heuristic( startcell, endcell )
    startcell.g = 0
    let cell = startcell
    let best = startcell
    let neighbour
    let cap = 500 // caps the exploration to a limited number of cells; when reached the current best path is returned
    for ( heap.push( startcell ); heap.size(); ) {
        cell = heap.pop()
        if ( cell.h < best.h ) best = cell
        if ( cell === endcell || 0 === cap-- ) break
        cell.closed = findpath.id
        let col = cell.col
        let row = cell.row
        let neighbours = [
            ASHES.cells.getColRow( col, row - 1 ),
            ASHES.cells.getColRow( col + 1, row ),
            ASHES.cells.getColRow( col, row + 1 ),
            ASHES.cells.getColRow( col - 1, row ),
            ASHES.cells.getColRow( col + 1, row - 1 ),
            ASHES.cells.getColRow( col + 1, row + 1 ),
            ASHES.cells.getColRow( col - 1, row + 1 ),
            ASHES.cells.getColRow( col - 1, row - 1 )
        ]
        for ( let i = 0, l = neighbours.length; i < l; i++ ) {
            neighbour = neighbours[ i ]
            if ( neighbour.closed === findpath.id ) continue
            cell.debug1 && ASHES.Object.setSprite( cell.debug1, 'debugpathfind' )
            let g = ASHES.Object.blockedByCells( object, ASHES.cells.overlapped( neighbour, object.width * object.sprite_scale_x, object.height * object.sprite_scale_y, object.rotation ) ) ? 0 : 1
            if ( 0 === g ) continue
            if ( neighbour === endcell && ASHES.Object.blockedByCell( object, endcell ) ) { best = neighbour; break }
            // skip diagonal neighbour if either adjoining neighbour is blocked
            if ( i === 4 && ( ASHES.Object.blockedByCell( object, neighbours[ 0 ] ) || ASHES.Object.blockedByCell( object, neighbours[ 1 ] ) ) ) continue
            if ( i === 5 && ( ASHES.Object.blockedByCell( object, neighbours[ 2 ] ) || ASHES.Object.blockedByCell( object, neighbours[ 1 ] ) ) ) continue
            if ( i === 6 && ( ASHES.Object.blockedByCell( object, neighbours[ 2 ] ) || ASHES.Object.blockedByCell( object, neighbours[ 3 ] ) ) ) continue
            if ( i === 7 && ( ASHES.Object.blockedByCell( object, neighbours[ 0 ] ) || ASHES.Object.blockedByCell( object, neighbours[ 3 ] ) ) ) continue
            g = cell.g + ( i < 4 ? 1 : 1.4142135623730951 ) * g
            let visited = neighbour.visited
            visited !== findpath.id && ( neighbour.h = heuristic( neighbour, endcell ), neighbour.g = 0 )
            if ( visited !== findpath.id || g < neighbour.g ) {
                neighbour.visited = findpath.id
                neighbour.parent = cell
                neighbour.g = g
                neighbour.score = neighbour.g + neighbour.h
                visited !== findpath.id ? heap.push( neighbour ) : heap.sinkDown( neighbour.index )
            }
        }
        if ( neighbour === endcell && ASHES.Object.blockedByCell( object, endcell ) ) break
    }
    let path = [ best === endcell ? end : best ]
    cell = best.parent
    while ( cell && cell.parent ) {
        cell.debug0 && ASHES.Object.setSprite( cell.debug0, 'debugpath' )
        path.push( cell )
        cell = cell.parent
    }
    return path
}

ASHES.cells.ray = ( a, b, endIfBlocked ) => {
    const interval = ASHES.cellsize
    let x = a.x
    let y = a.y
    let vx = b.x - x
    let vy = b.y - y
    if ( 0 === vx && 0 === vy ) return [ ASHES.cells.getXY( x, y ) ]
    let l = vx * vx + vy * vy
    let cells = []
    let dx = interval * ( vx / Math.sqrt( l ) )
    let dy = interval * ( vy / Math.sqrt( l ) )
    vx = 0
    vy = 0
    while ( true ) {
        let cell = ASHES.cells.getXY( x + vx, y + vy )
        cell.debug1 && ASHES.Object.setSprite( cell.debug1, 'debugray' )
        if ( cells.indexOf( cell ) < 0 ) cells.push( cell )
        if ( endIfBlocked && ASHES.Object.blockedByCell( endIfBlocked, cell ) ) {
            cell._ray_info = { a: a, b: b, x: x + vx, y: y + vy, dx: dx, dy: dy }
            return cells
        }
        vx += dx
        vy += dy
        if ( vx * vx + vy * vy >= l ) {
            cell = ASHES.cells.getXY( b.x, b.y )
            cell.debug1 && ASHES.Object.setSprite( cell.debug1, 'debugray' )
            cells.indexOf( cell ) < 0 && cells.push( cell )
            if ( endIfBlocked && ASHES.Object.blockedByCell( endIfBlocked, cell ) )
                cell._ray_info = { a: a, b: b, x: x + vx, y: y + vy, dx: dx, dy: dy }
            break
        }
    }
    return cells
}



ASHES.canvas = document.createElement( 'canvas' )
ASHES.canvas.style.width = '100%'
ASHES.canvas.style.height = '100%'
document.body.appendChild( ASHES.canvas )


ASHES.ctx = ASHES.canvas.getContext( '2d', { alpha: false } )
ASHES.ctx.imageSmoothingEnabled = false



ASHES.viewport = {
    x: 0,
    y: 0,
    width: 0,
    height: 0,
    rotation: 0,
    scale: 1,
    overdraw: 100,
    lastupdate: -100,
    dirty: true,
    needsupdate: true,
    firstrow: 0,
    lastrow: 0,
    firstcol: 0,
    lastcol: 0
}


ASHES.viewport.update = () => {

    ASHES.mouse.world.x = ( ASHES.mouse.screen.x - .5 * ASHES.canvas.width ) / ASHES.viewport.scale + ASHES.viewport.x
    ASHES.mouse.world.y = ( ASHES.mouse.screen.y - .5 * ASHES.canvas.height ) / ASHES.viewport.scale + ASHES.viewport.y

    ASHES.viewport.width = ( ASHES.canvas.width + 2 * ASHES.viewport.overdraw ) / ASHES.viewport.scale
    ASHES.viewport.height = ( ASHES.canvas.height + 2 * ASHES.viewport.overdraw ) / ASHES.viewport.scale

    let halfheight = .5 * ASHES.viewport.height
    let halfwidth = .5 * ASHES.viewport.width

    let firstrow = Math.round( ( ASHES.viewport.y - halfheight ) / ASHES.cellsize )
    let lastrow = Math.round( ( ASHES.viewport.y + halfheight ) / ASHES.cellsize )
    
    let firstcol = Math.round( ( ASHES.viewport.x - halfwidth ) / ASHES.cellsize )
    let lastcol = Math.round( ( ASHES.viewport.x + halfwidth ) / ASHES.cellsize )

    if ( firstrow === ASHES.viewport.firstrow && lastrow === ASHES.viewport.lastrow && firstcol === ASHES.viewport.firstcol && lastcol === ASHES.viewport.lastcol ) return

    ASHES.viewport.firstrow = firstrow
    ASHES.viewport.lastrow = lastrow
    ASHES.viewport.firstcol = firstcol
    ASHES.viewport.lastcol = lastcol


    for ( let i = 0; i < ASHES.cells.inview.length; i++ )
        ASHES.cells.inview[ i ].remove = true

    for ( let row = firstrow; row <= lastrow; row++ ) {
        for ( let col = firstcol; col <= lastcol; col++ ) {
            let cell = ASHES.cells.getColRow( col, row )
            cell.remove = false
            if ( !cell.inview ) {
                cell.inview = true
                ASHES.cells.inview.push( cell )
                for ( let t = 0, l = cell.children.length; t < l; t++ )
                    ASHES.Object.addToView( cell.children[ t ] )
            }
        }
    }

    for ( let i = 0; i < ASHES.cells.inview.length; i++ ) {
        let cell = ASHES.cells.inview[ i ]
        if ( cell.remove ) {
            cell.inview = false
            cell.remove = false
            ASHES.cells.inview.splice( i, 1 )
            i--
            for ( let t = 0, l = cell.children.length; t < l; t++ )
                ASHES.Object.removeFromView( cell.children[ t ] )
        }
    }

    ASHES.Object.inview.sort( comparator )

}


ASHES.viewport.setOverdraw = overdraw => {
    ASHES.viewport.overdraw = overdraw
}


ASHES.viewport.set = ( x, y ) => {
    if ( x === ASHES.viewport.x && y === ASHES.viewport.y ) return
    ASHES.viewport.x = x
    ASHES.viewport.y = y
}


ASHES.viewport.follow = object => {
    ASHES.following = object
    ASHES.viewport.set( object.x, object.y )
}


ASHES.viewport.zoom = zoom => {
    ASHES.viewport.scale = zoom
}





ASHES.Animation.start = ( duration, stepCallback, onComplete ) => {
    
    let animation = {
        id: ASHES.nextGUID++,
        start: ASHES.time,
        stopped: false,
        task: () => {
            if ( animation.stopped ) return true
            let lerp = ( ASHES.time - animation.start ) / duration
            if ( 0 >= duration || 1 <= lerp ) lerp = 1
            if ( stepCallback && stepCallback( lerp ) || 1 === lerp ) {
                ASHES.Animation.stop( animation.id )
                if ( onComplete ) onComplete()
                return true
            }
        }
    }
    
    tasks.push( animation.task )

    ASHES.animations[ animation.id ] = animation
    return animation.id
}


ASHES.Animation.stop = ( id ) => {

    if ( ASHES.animations.hasOwnProperty( id ) ) {
        let animation = ASHES.animations[ id ]
        animation.stopped = true
        delete ASHES.animations[ id ]
    }

}




let stats = Stats()
let objects_stats = stats.addPanel( new Stats.Panel( 'Objects', '#ff8', '#221' ) )

for ( let i = 0; i < stats.dom.children.length; i++ ) {
   stats.dom.children[ i ].style.display = 'inline-block'
   stats.dom.children[ i ].style.position = 'relative'
}

document.body.appendChild( stats.dom )


const tasks = [
    //() => { ASHES.Event.dispatch( ASHES, 'update' ) }
]
tasks.i = 0




function update() {

    stats.begin();

    let t = performance.now()

    ASHES.elapsed = t - ASHES.time
    ASHES.time = t

    ASHES.viewport.update()

    if ( tasks.i < tasks.length ) {
        if( tasks[ tasks.i ]() ) {          
            tasks.splice( tasks.i, 1 )
        } else {
            tasks.i++
        }
        if ( tasks.i >= tasks.length ) tasks.i = 0
    }
    
    if ( ASHES.viewport.dirty ) {

        ASHES.viewport.dirty = false

        ASHES.ctx.save()
        
        ASHES.canvas.width = ASHES.canvas.width
        ASHES.ctx.fillStyle = '#081c3a'
        ASHES.ctx.fillRect( 0, 0, ASHES.canvas.width, ASHES.canvas.height )
        ASHES.ctx.translate( .5 * ASHES.canvas.width, .5 * ASHES.canvas.height )
        ASHES.viewport.rotation && ASHES.ctx.rotate( ASHES.viewport.rotation )
        ASHES.viewport.scale !== 1 && ASHES.ctx.scale( ASHES.viewport.scale, ASHES.viewport.scale )
        ASHES.ctx.translate( - ASHES.viewport.x, - ASHES.viewport.y )
        
        for ( let i = 0; i < ASHES.Object.inview.length; i++ ) {
            let object = ASHES.Object.inview[ i ]
            //ASHES.Event.dispatch( object, 'preRender', object )
            ASHES.Object.draw( object )
        }

        ASHES.ctx.restore()
        
    }
    
    objects_stats.update( ASHES.Object.inview.length, 5000 )

    stats.end()

}


setInterval( update, 0 )


window.addEventListener( 'mousemove', e => {
    if ( 0 === ASHES.mouse.screen.x - e.x && 0 === ASHES.mouse.screen.y - e.y ) return
    ASHES.mouse.screen.x = e.x / window.innerWidth * ASHES.canvas.width
    ASHES.mouse.screen.y = e.y / window.innerHeight * ASHES.canvas.height
    ASHES.mouse.world.x = ( ASHES.mouse.screen.x - .5 * ASHES.canvas.width ) / ASHES.viewport.scale + ASHES.viewport.x
    ASHES.mouse.world.y = ( ASHES.mouse.screen.y - .5 * ASHES.canvas.height ) / ASHES.viewport.scale + ASHES.viewport.y
    ASHES.Event.dispatch( ASHES, 'mousemove', ASHES.mouse )
} )

window.addEventListener( 'mousedown', e => {
    if ( 0 === e.button ) ASHES.mouse.left = true
    else if ( 1 === e.button ) ASHES.mouse.middle = true
    else if ( 2 === e.button ) ASHES.mouse.right = true
    ASHES.Event.dispatch( ASHES, 'mousedown', ASHES.mouse )
} )

document.body.addEventListener( 'touchstart', e => {
    let touchobj = e.changedTouches[ 0 ] // reference first touch point (ie: first finger)
    let x = parseInt( touchobj.clientX ) // get x position of touch point relative to left edge of browser
    let y = parseInt( touchobj.clientY ) // get x position of touch point relative to left edge of browser
    if ( 0 === ASHES.mouse.screen.x - x && 0 === ASHES.mouse.screen.y - y ) return
    ASHES.mouse.screen.x = x
    ASHES.mouse.screen.y = y
    ASHES.mouse.world.x = ( ASHES.mouse.screen.x - .5 * ASHES.canvas.width ) / ASHES.viewport.scale + ASHES.viewport.x
    ASHES.mouse.world.y = ( ASHES.mouse.screen.y - .5 * ASHES.canvas.height ) / ASHES.viewport.scale + ASHES.viewport.y
    ASHES.Event.dispatch( ASHES, 'mousemove', ASHES.mouse )
    ASHES.mouse.left = true
    ASHES.Event.dispatch( ASHES, 'mousedown', ASHES.mouse )
    e.preventDefault()
}, false )

document.body.addEventListener( 'touchend', function ( e ) {
    var touchobj = e.changedTouches[ 0 ] // reference first touch point for this event
    ASHES.mouse.left = false
    ASHES.Event.dispatch( ASHES, 'mouseup', ASHES.mouse )
    e.preventDefault()
}, false )

window.addEventListener( 'mouseup', e => {
    if ( 0 === e.button ) ASHES.mouse.left = false
    else if ( 1 === e.button ) ASHES.mouse.middle = false
    else if ( 2 === e.button ) ASHES.mouse.right = false
    ASHES.Event.dispatch( ASHES, 'mouseup', ASHES.mouse )
} )

window.addEventListener( 'contextmenu', e => {
    e.stopPropagation()
    e.preventDefault()
    ASHES.Event.dispatch( ASHES, 'contextmenu', ASHES.mouse )
} )




let initseed = ASHES.randomString( 16 )
console.log( 'seed', initseed )
ASHES.noise.seed( initseed )

let debugcell = ASHES.Sprite( { name: 'debugcell', fill: '#000000', width: 5, height: 5 } )
// ASHES.Sprite( { name: 'debugpath', fill: '#229922', width: ASHES.cellsize, height: ASHES.cellsize } )
let debugweight = ASHES.Sprite( { name: 'debugweight', fill: '#000000', width: ASHES.cellsize, height: ASHES.cellsize } )
// ASHES.Sprite( { name: 'debugintersecthit', fill: '#ff5500', width: 5, height: 5 } )
// ASHES.Sprite( { name: 'debugintersectmiss', fill: '#55ff00', width: 5, height: 5 } )
// ASHES.Sprite( { name: 'debugray', fill: '#0055ff', width: 5, height: 5 } )
// ASHES.Sprite( { name: 'debugpathfind', fill: '#ff55ff', width: 5, height: 5 } )

ASHES.Event.addListener( ASHES, 'newcell', cell => {
    // cell.debug0 = ASHES.Object.add( { x: cell.x, y: cell.y, layer: 98, opacity: .5 } )
    //cell.debug1 = ASHES.Object.add( { x: cell.x, y: cell.y, layer: 99, opacity: .5 } )
} )

ASHES.Event.addListener( ASHES, 'cellchildrenchanged', cell => {
    cell.debug0 && ASHES.Object.setSprite( cell.debug0, cell.physical ? debugweight : debugcell )
} )

export { ASHES as default }
