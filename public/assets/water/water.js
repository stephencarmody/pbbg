import ASHES from '../../ashes.js'

function onnewcell( cell ) {
    
    if ( !sprites ) return
    
    if ( ASHES.Vec2.len( cell ) < 100 ) return

    let n = __game__.noise( cell.x, cell.y, 30000 )
    // let n50 = __game__.noise( cell.x, cell.y, 50 )
    // let n20000 = __game__.noise( cell.x, cell.y, 20000 )
    // let n30000 = __game__.noise( cell.x, cell.y, 30000 )
    // let n60000 = __game__.noise( cell.x, cell.y, 60000 )
    
    // let rotation = ( n30000 + 1 ) * Math.PI
    
    // if ( __game__.gridalign( cell, 32 ) ) {
    //     if ( n50 < -.2 ) return Water( cell )
    if ( n > 0.75 ) {
        // let n = __game__.noise( cell.x, cell.y, 30000 )
        // let water = ASHES.Object.add( { sprite: n > 0 ? 'water0' : 'water1', x: cell.x, y: cell.y, opacity: .6, rotation: ( n + 1 ) * Math.PI, layer: 1 } )
        // ASHES.Event.addListener( water, 'preRender', () => {
        //     let positional_noise = __game__.noise( water.x + ASHES.time, water.y + ASHES.time, 1 )
        //     water.rotation_delta = ( positional_noise * .7 + __game__.global_n * .3 ) * 2
        //     water.sprite_scale_x_delta = water.sprite_scale_y_delta = ( 1 + positional_noise * .5 + __game__.global_n * .5 ) * 20
        // } )
    }

}

let sprites

ASHES.Event.addListener( ASHES, 'newcell', onnewcell )

ASHES.Sprite.loadSprites( [ '/assets/water/water0.png', '/assets/water/water1.png' ], ( loadedsprites, new_cells_before_ready ) => {

    sprites = loadedsprites

    for ( let i = 0; i < new_cells_before_ready.length; i++ ) {
        onnewcell( new_cells_before_ready[ i ] )
    }

} )
