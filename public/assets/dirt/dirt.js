import ASHES from '../../ashes.js'

const sprite = ASHES.Sprite( { name: 'dirt', image: './assets/dirt/dirt0.png' } )

const new_cells_before_ready = []

ASHES.Event.addListener( ASHES, 'newcell', cell => {
    if (!sprite.ready)
        return new_cells_before_ready.push( cell )
    if ( cell.x % sprite.image.width === 0 && cell.y % sprite.image.height === 0 ) {
        ASHES.Object.add( { sprite: sprite, x: cell.x, y: cell.y, layer: -2 } )
    }
} )

ASHES.Event.addListener( sprite, 'ready', sprite => {
    while (new_cells_before_ready.length) {
        let cell = new_cells_before_ready.pop()
        if ( cell.x % sprite.image.width === 0 && cell.y % sprite.image.height === 0 ) {
            ASHES.Object.add( { sprite: sprite, x: cell.x, y: cell.y, layer: -2 } )
        }
    }
} )
