import ASHES from '../../ashes.js'

function onnewcell( cell ) {
    
    if ( !sprites ) return

    if ( ASHES.Vec2.len( cell ) < 10 ) return

    let n = __game__.noise( cell.x, cell.y, 30000 )
    
    if ( n > 0.85 ) {
        let sprite = sprites[ 0 ]
        ASHES.Object.add( {
            sprite: sprite,
            x: cell.x,
            y: cell.y,
            //sprite_scale_x: 3 + __game__.noise( cell.x / 100, cell.y / 100, 100 ) * 2,
            //sprite_scale_y: 3 + __game__.noise( cell.x + 1000, cell.y + 1000, 100 ) * 2,
            physical: true,
            //rotation: __game__.noise( cell.x + 500, cell.y + 500, 100 ) > 0 ? 0 : -Math.PI,
            width: sprite.image.width * .8,
            height: sprite.image.height * .8,
            layer: -1
        } )
    }

}

let sprites

ASHES.Event.addListener( ASHES, 'newcell', onnewcell )

ASHES.Sprite.loadSprites( [ './assets/rock/rock1.png' ], ( loadedsprites, new_cells_before_ready ) => {

    sprites = loadedsprites

    for ( let i = 0; i < new_cells_before_ready.length; i++ ) {
        onnewcell( new_cells_before_ready[ i ] )
    }

} )
